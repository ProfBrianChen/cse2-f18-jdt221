//////////
// CSE 02 Welcome Classs

public class WelcomeClass{
  
  public static void main(String agrs[]){
    //print the instructed text on terminal wondow
    //print on new line every time
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); //Backslash is an escape character, so an additional backslash is needed next to every backslash to print every backslash 
    System.out.println("<-J--D--T--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); //Backslash is an escape character, so an additional backslash is needed next to every backslash to print every backslash 
    System.out.println("  v  v  v  v  v  v");
  }
  
}