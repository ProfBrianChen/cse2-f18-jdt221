/////////
// CSE 002 User Input
// lab05
//Jorgo Damtew Tesfa, 10-08-2018
//
import java.util.Scanner;

public class UserInput {
    public static void main (String [] args){
        
      Scanner scan = new Scanner(System.in);
        int numMeetPerWeek, numStudent, classTime, courseNum;
        int timeCheck = 0; // a variable used to check if the time written in military time or not. see below
        String deptName, instructorName;
        String unwanted; // to store the unwanted inputs
        
        System.out.println("This is regarding a course you are currently taking.");
        
        System.out.print("Enter the course number: ");
        while (!scan.hasNextInt()){
            System.out.println("That is not a number. Try again");
            System.out.print("Enter the course number: ");
            unwanted = scan.next();
        }
        courseNum = scan.nextInt(); //course number is assigned
        
      
        System.out.print("Enter the department name: ");
        while (!scan.hasNext() || scan.hasNextInt() || scan.hasNextDouble() || scan.hasNextFloat()){
            System.out.println("That is not a department name.");
            System.out.print("Enter the department name: ");
            unwanted = scan.next();
        }
        deptName = scan.next(); //department name is assigned 
        
      
        System.out.print("Enter number of times the class meets in a week: ");
        while (!scan.hasNextInt()){
            System.out.println("That is not an integer. Try again.");
            System.out.print("Enter number of times the class meets in a week: ");
            unwanted = scan.next();
        }
        numMeetPerWeek = scan.nextInt(); //number of meeting per week is assigned
        
       
        System.out.print("Enter the time the class meets (military time, as in 0000-2359): ");
        while (!scan.hasNextInt()){
            System.out.println("That is not military time. Try again.");
            System.out.print("Enter the time the class meets (military time, as in 0000-2359): ");
            unwanted = scan.next();
        }
        while (scan.hasNextInt()){
        timeCheck = scan.nextInt();
        if (timeCheck < 0 || timeCheck > 2359){
            System.out.println("That is not military time. Try again.");
            System.out.print("Enter the time the class meets (military time, as in 0000-2359): ");
            unwanted = Integer.toString(timeCheck);
        }
        else if (timeCheck >= 0 && timeCheck <= 2359){
            if (timeCheck >= 100){
                int minute = timeCheck % ((timeCheck/100)*100); //initializing value to check the minutes
                if (minute >= 0 && minute <= 59){ //check the minutes (bacause we do not want something like 2398)
                    break;
                }
                else{
                    System.out.println("That is not military time. Try again.");
                    System.out.print("Enter the time the class meets (military time, as in 0000-2359): ");
                    unwanted = Integer.toString(timeCheck);
                }
            }
            else if (timeCheck < 100){ //this is done to avoid deviding by zero on line 61
                int minute = timeCheck;
                if (minute >= 0 && minute <= 59){ //check the minutes (bacause we do not want something like 2398)
                    break;
                }
                else{
                    System.out.println("That is not military time. Try again.");
                    System.out.print("Enter the time the class meets (military time, as in 0000-2359): ");
                    unwanted = Integer.toString(timeCheck);
                }
            }
        }
        }
        classTime = timeCheck; //military time is assigned
        
        
        System.out.print("Enter the name of the instructor: ");
        while (scan.hasNextInt() || scan.hasNextDouble() || scan.hasNextFloat() || !scan.hasNext()){
            System.out.println("That is not a name. Try again.");
            System.out.print("Enter the name of the instructor: ");
            unwanted = scan.next();
        }
        instructorName = scan.next(); //instructor name is assigned
        
        System.out.print("Enter the number of students in the class: ");
        while (!scan.hasNextInt()){
            System.out.println("That is not an integer. Try again.");
            System.out.print("Enter the number of students in the class: ");
            unwanted = scan.next();
        }
        numStudent = scan.nextInt(); // number of students per class is assigned 
        
        System.out.println("Course Number: " + courseNum);
        System.out.println("Department Number: " + deptName);
        System.out.println("Meetings per week" + numMeetPerWeek);
        System.out.println("Time of class: " + classTime);
        System.out.println("Name od istructor: " + instructorName);
        System.out.println("Number of students: " + numStudent);
        
    }
}