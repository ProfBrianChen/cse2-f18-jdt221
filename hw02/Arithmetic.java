/////////
// CSE 002 Arithmetic
// hw02 by Jorgo Damtew Tesfa, 09-10-2018

public class Arithmetic{
  
  public static void main(String [] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostPants = numPants * pantsPrice, //total cost of pants, quantity of pants multiplied by price of pants
    totalCostShirts = numShirts * shirtPrice, //total cost of shirts, quantity of shirts multiplied by price of shirts
    totalCostBelts = numBelts * beltCost, //total cost of belts, quantity of belts multiplied by price of belts
    taxOnPants = totalCostPants * paSalesTax, // amount of tax on pants, cost of puchase for pants times PA tax rate
    taxOnShirts = totalCostShirts * paSalesTax, // amount of tax on shirts, cost of puchase for shirts times PA tax rate
    taxOnBelts = totalCostBelts * paSalesTax; // amount of tax on belts, cost of puchase for belts times PA tax rate
    
    System.out.print("\nThe total cost of " + numPants + " pant(s) is $" + totalCostPants + ".\n");
    System.out.print("The total cost of " + numShirts + " shirt(s) is $" + totalCostShirts + ".\n");
    System.out.print("The total cost of " + numBelts + " belt(s) is $" + totalCostBelts + ".\n");
    
    double costPurchaseNoTax = totalCostPants + totalCostShirts + totalCostBelts, // total cost of purchase without tax
    totalSalesTax = taxOnPants + taxOnShirts + taxOnBelts, // total amount of tax on all the items
    costPurchaseWithTax = costPurchaseNoTax + totalSalesTax; // total cost of purchase with tax
    
    
    System.out.print("Excluding tax, the total cost of purchase is $" + costPurchaseNoTax + ".\n");
    System.out.printf("The total sales tax is $%.2f.\n", totalSalesTax);
    System.out.printf("Therefore, including sales tax, the toal cost of purchase is $%.2f.\n\n", costPurchaseWithTax);
     

  }
}