/*
Jorgo Damtew Tesfa
jdt221
11/14/2018
 */
public class Arrays {
    
    public static int[] array(){
        int[] array = new int[100]; //declaring an array
        
        for (int n=0; n<100; n++){
            int random = (int) (Math.random()*100); //generating the array with random numbers form 0-99
            array[n] = random; //assigning each random number as each element
        }
        return array;
    }
    
    public static void countArray(){
        int[] array = array(); // calling the array
        int[] count = new int[100]; //declating a counter array
        int countOcc;
                
        System.out.print("Array 1 holds the following integers: ");
        for (int n=0; n<100; n++){
            System.out.print(array[n] + " "); //printing the elements of the array
        }
        System.out.println();

        for (int n = 0; n<100 ; n++){
            countOcc = 0;
            for (int i = 0; i<100; i++){
                if (n== array[i]){
                    countOcc++; //counting the occurances of the array
                }
                
            }
            count[n] = countOcc; //assigning the counts as each element of the counter array
        }
        
        
        for (int p=0; p<100; ++p){
          //printingt he number of occurances of each number from 0-99 that appears more than 1 time  
          if (count[p]==1){
                System.out.println(p + " occurs " + count[p] + " time."); 
            }
            else if (count[p]>1){
                System.out.println(p + " occurs " + count[p] + " times.");
            }
        }
    }
    
    public static void main(String[] args) {
        countArray(); //calling the methon in the main method to excute
    }
    
}