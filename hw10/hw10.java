/**
 *Jorgo Damtew Tesfa
 * 12-03-18
 * HW 10, Tic-Tac-Toe
 */
import java.util.Scanner;
public class hw10 {

    public static boolean linearSearch(char[][] list, char key) { 
        //searched the entered key in the array
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++ ){
                //if found, return true
                if (key == list[i][j] && key != 'x' && key != 'o') return true;
            }   
	}
        //if not found return false
	return false;
    }
    
    public static boolean checkWin(char [][] array){ //checking if one of the players have won the game
        //return true if there is a winner
        //check rows to see if there is a winner
        for (int i = 0; i <3; i++){
            if(array[i][0]==array[i][1] && array[i][1]==array[i][2]) return true;
        }
        //check columns to see if there is a winner
        for (int i = 0; i <3; i++){
            if(array[0][i]==array[1][i] && array[1][i]==array[2][i]) return true;
        }
        //check diagonals to see if there is a winner
        if (array[0][0]==array[1][1] && array[1][1]==array[2][2] ||
            array[0][2]==array[1][1] && array[1][1]==array[2][0]) return true;
        //if there is no winner return false
        return false;
    }
    
    public static boolean checkDraw(char[][] array){
        //search for a number. if there is one, there are more rounds to go
        //so not a draw. But if all are x and o, and no winner, then it's a draw
        for (int number =1; number <=9; number ++){
            if(linearSearch(array, Integer.toString(number).charAt(0))) return false;
        }
        return true;
    }
    
    public static void printArray(char[][] array){
        //print all values of the array
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                System.out.print(Character.toString(array[i][j]) + "  ");
            }
            System.out.println();
        }
    }
    
    public static boolean turn(char player, char opponent, char[][] array){
        Scanner scan = new Scanner(System.in);
        char position; //refering to location in the array
        //check is there is a winner before poceeding 
        if (checkWin(array)){
            printArray(array);
            System.out.println("The " + opponent + "-player has won the game.");
            return false;//return false so that main method does not excecute anymore
        } 
        //check if there is a draw, if there is no winner
        else if(checkDraw(array)){
            printArray(array);
            System.out.println("The game has ended in a draw.");
            return false; //return false so that main method does not excecute anymore
        }
        //if no win, no draw, then ask the player to input a position value
        else {
            printArray(array);
            System.out.println("It is " + player + "-player's turn.");
            System.out.print("Choose the position of you wnat to conquer: ");
            char input = scan.next().charAt(0);
            //keep asking user if input in invalid
                //input is invalid if the number (representing the position is not in the array
            while(!linearSearch(array, input)){
                System.out.println("Invalid Entry.");
                System.out.println("Choose the position of you want to conquer: ");
                input = scan.next().charAt(0);
            }
            position = input;//position that the player wants to play
            
            switch(position){ //change the value at position based on input
                //value of player is x when the turn is that of the x-player and vice versa
                case  '1': array[0][0] = player;  
                break;
                case  '2': array[0][1] = player;
                break;
                case  '3': array[0][2] = player;
                break;
                case  '4': array[1][0] = player;
                break;
                case  '5': array[1][1] = player;
                break;
                case  '6': array[1][2] = player;
                break;
                case  '7': array[2][0] = player;
                break;
                case  '8': array[2][1] = player;
                break;
                case  '9': array[2][2] = player;
                break;
                default: break;
            }
            return true; //return true so that main method keeps going in the while loop
        }
    }
    
    public static void main(String[] args) {
        char [][] ticTac = new char[3][3]; //create a 3*3 array
        char xPlayer = 'x'; //the x-player uses the char x
        char oPlayer = 'o'; //the o-player uses the char o
        int number = 1; //to fill in the array
        //fill the array with numbers from 1 to 9 (inclusive)
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                ticTac[i][j] = Integer.toString(number).charAt(0);
                number++;
            }
        }
        
        while (true){ //keeps executing until it breaks
            //x-player's turn
            if (!turn(xPlayer, oPlayer, ticTac)) break;
            //o-player's trun
            if (!turn(oPlayer, xPlayer, ticTac)) break;
        }
    }
}