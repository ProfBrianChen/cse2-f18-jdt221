/**
 *lab 07, sentence and paragraph maker
 * Jorgo Damtew Tesfa jdt221
 */
import java.util.Random;
import java.util.Scanner;
public class lab07 {

    public static String adjective(){ //method to randomly generate adjectives
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String adjective = " ";
        switch (randomInt){
            case 0: adjective = "clever ";
                break;
            case 1: adjective = "bored ";
                break;
            case 2: adjective = "hungry ";
                break;
            case 3: adjective = "flippant ";
                break;
            case 4: adjective = "husky ";
                break;
            case 5: adjective = "bright ";
                break;
            case 6: adjective = "colorful ";
                break;
            case 7: adjective = "beautiful ";
                break;
            case 8: adjective = "repulsive ";
                break;
            case 9: adjective = "stincky ";
                break;  
        }
        return adjective;
    }
    
    public static String subjectNoun(){ //method to randomly generate subjects
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        
        String subjectNoun = " ";
        switch (randomInt){
            case 0: subjectNoun = "squirrel ";
                break;
            case 1: subjectNoun = "ant ";
                break;
            case 2: subjectNoun = "computer ";
                break;
            case 3: subjectNoun = "professor ";
                break;
            case 4: subjectNoun = "athlete ";
                break;
            case 5: subjectNoun = "sister-in-law ";
                break;
            case 6: subjectNoun = "pig ";
                break;
            case 7: subjectNoun = "jet ";
                break;
            case 8: subjectNoun = "dancer ";
                break;
            case 9: subjectNoun = "midwife ";
                break;
        }
        return subjectNoun;
    }
    
    public static String pastVerb(){ //method to randomly generate past tense verbs
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        
        String pastVerb = " ";
        switch (randomInt){
            case 0: pastVerb = "grabbed ";
                break;
            case 1: pastVerb = "broke ";
                break;
            case 2: pastVerb = "passed ";
                break;
            case 3: pastVerb = "crushed ";
                break;
            case 4: pastVerb = "hugged ";
                break;
            case 5: pastVerb = "licked ";
                break;
            case 6: pastVerb = "saw ";
                break;
            case 7: pastVerb = "kicked ";
                break;
            case 8: pastVerb = "kissed ";
                break;
            case 9: pastVerb = "copulated ";
                break;
        }
        return pastVerb;
    }
    
    public static String objectNoun(){ //method to randomly generate objects
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        
        String objectNoun = " ";
        switch (randomInt){
            case 0: objectNoun = "squirrel";
                break;
            case 1: objectNoun = "ant";
                break;
            case 2: objectNoun = "computer";
                break;
            case 3: objectNoun = "professor";
                break;
            case 4: objectNoun = "athlete";
                break;
            case 5: objectNoun = "sister-in-law";
                break;
            case 6: objectNoun = "pig";
                break;
            case 7: objectNoun = "jet";
                break;
            case 8: objectNoun = "dancer";
                break;
            case 9: objectNoun = "midwife";
                break;
        }
        return objectNoun;
    }
    
    public static String sentenceMaker(){ //method for making a thesis statement
        Scanner scan = new Scanner(System.in);
        String subject = subjectNoun();
        String thesisSentence;
        
        //making the thesis statement
        thesisSentence = "The " + adjective() + subject + pastVerb() + "the " + adjective() + objectNoun() + ".";
        System.out.println ("\n"+thesisSentence);
        
        //ask the suer it the user wnats another sentence
        System.out.println("- Do you want another sentence? Press 1 for yes, 0 for no.");
      
        //integer entrys are not accepted
        while (!scan.hasNextInt()){
            System.out.print("Invalid entry.\n- Do you want another sentence? Press 1 for yes, 0 for no.");
            String junk = scan.next();
        }
        while (scan.hasNextInt()){
            int entry = scan.nextInt();
          
            //integers other than 1 and 0 are not accepted
            if (entry <0 || entry >1){
                System.out.print("Invalid entry.\n- Do you want another sentence? Press 1 for yes, 0 for no.");
                String junk = Integer.toString(entry);
            }
            else if (entry==1){
                subject = subjectNoun();
                thesisSentence = "The " + adjective() + subject + pastVerb() + "the " + adjective() + objectNoun() + ".";
                System.out.println (thesisSentence);
                System.out.println("-> Do you want another sentence? Press 1 for yes, 0 for no."); // ask user again
                continue; // take user input again
            }
            else if (entry==0) {
                break;
            }
        }
        
        return subject; //return the subject for future user
    }
    
    
    public static void actionSentenceAndConc(){
        String subject = sentenceMaker();
        //calling sentenceMaker also prints thesis sentnece and asks user for another sentence 
        
        //make actions sentence
        String actionSentence = "This " + subject + "also " + pastVerb() + "the " + adjective() + objectNoun() +".";
        //make conclusion sentnece
        String conclusion = "However, that " + subject + "never " + pastVerb() + "any " + objectNoun() +"."; 
        
        //print the action and conclusion
        System.out.println(actionSentence + "\n" + conclusion);
        
    }
    
    public static void main(String[] args) {
        
        actionSentenceAndConc();
        //this prints the thesis, asks the user for another sentnece the action sentnece and the conclusion
    }
}
