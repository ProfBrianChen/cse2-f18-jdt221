//////////
/// CSE 02 Lab 02 Cyclimeter
// Jorgo Tesfa
// September 13, 2018
import java.util.Scanner;
public class Check{
  public static void main(String [] args){
    Scanner myScanner = new Scanner( System.in ); 
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble(); //scanning the input for cost of the check
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble(); //scanning the input for percent of tip
    tipPercent /= 100; //convert the percentage to decimal values
    
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //number of people who had dinner

    double totalCost, //total cost that every person has to pay
      costPerPerson; // amount that each person has to pay
    
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits to the right of the decimal point for the cost$
    
    totalCost = checkCost * (1 + tipPercent); //total cost including tips
    costPerPerson = totalCost / numPeople; //amount that every person pays
    
    dollars = (int)costPerPerson; //get the whole amount, dropping decimal fraction
    dimes = (int)(costPerPerson * 10) % 10; //finding the tenth decimal place
    pennies = (int)(costPerPerson * 100) % 10; //finding the hundredth decimal pace
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  }
}