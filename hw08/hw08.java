/*Jorgo Damtew Tesfa
jdt221
11/14/2018
*/
import java.util.Scanner;

public class hw08{ 

    public static String[] getHand(String[] list, int index, int numCards){
        String hand[] = new String [numCards];
        int p = 0; // index for the array hand[p]
        //get the last cards in the suffled deck (index-numCards)
        for (int k =index ; k > (index - numCards); k--){
            hand[p] = list[k];
            p++;
        }
        System.out.println("Hand");
        return hand;
    }

    public static void printArray(String[] list){
        int length = list.length;
        for(int i=0; i < length; i++){
            //print every value with space in between
            System.out.print(list[i] + " ");
        }
        System.out.println();
    }
    
    public static String[] shuffle(String[] list){
        String tempHold = " "; //temporary value holder to use to swap cards
        int randIndex = 0;
        for (int i = 0; i < 100; i++){ //swapping done 100 times becomes good shuffling
            randIndex = (int) (Math.random() * 51 + 1);
            //swap the first card in the deck witht a random card in the deck
            tempHold = list[0];
            list[0] = list[randIndex];
            list[randIndex] = tempHold;
        }
        System.out.println("Shuffled");
        return list;
    }

    public static void main(String[] args) { 

        Scanner scan = new Scanner(System.in); //suits club, heart, spade or diamond 
        String[] suitNames={"C","H","S","D"};    
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
        String[] cards = new String[52]; 
        String[] hand = new String[5]; 

        int numCards; //declaring number of cards in a hand
        int again = 1; //declaring variable for cheking if the user another hand
        int index = 51; //index for the deck

        for (int i=0; i<52; i++){ 
            //making the deck of cards with numbers and suits
            cards[i]=rankNames[i%13]+suitNames[i/13]; 
        } 
        printArray(cards); //printing the deck
        shuffle(cards);  //shuffling the cards
        printArray(cards); //printing the shuffled deck
        
        System.out.println("Enter desired number of card in a hand: "); 
            numCards = scan.nextInt(); //number of cards in the hand assigned
            
        while(again == 1){ 
            if (index+1-numCards >= 0) { //make sure there enough cards in the deck
                hand = getHand(cards,index,numCards); 
                printArray(hand);
                index = index - numCards;
                System.out.println("Enter a 1 if you want another hand drawn"); 
                again = scan.nextInt();
            }
            else { //if there not enough cards in a deck for a/another hand
                System.out.println("Not enough cards in the deck for a hand. Good bye.");
                break;
            }
        }  
    } 
}
