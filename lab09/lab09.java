/**
 * Jorgo Damtew Tesfa
 * jdt221
 * 11/15/2018
 */
public class lab09 {
    
    public static int[] copy(int[] original){
        int length = original.length;
        int [] copy = new int[length];
        
        for (int i = 0; i < length; i++){
            copy[i] = original[i];
        }
        
        return copy;
    }
    
    public static void inverter(int[] original){
        int length = original.length;
        int [] invert = new int[length];
        
        for (int i = 0; i < length; i++){
            invert[i] = original[length-1-i];
        }
        
        for (int i = 0; i < length; i++){
            original[i] = invert[i];
        }
    }
    
    public static int[] inverter2(int[] original){
        int[] copy = copy(original);
        inverter(copy);
        return copy;
    }
    
    public static void print(int[] original){
        System.out.print("Members of the array are: ");
        int length = original.length;
        for (int i = 0; i < length; i++){
            System.out.print(original[i] + " ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        int[] array0 = {8, 64, 7, 9, 55, 68, 10, 82, 6, 31, 0};
        int[] array1 = copy(array0);
        int[] array2 = copy(array0);
        
        inverter(array0);
        print(array0);
        
        inverter2(array1);
        print(array1);
        
        int[] array3 = inverter2(array2);
        print(array3);
    }
}