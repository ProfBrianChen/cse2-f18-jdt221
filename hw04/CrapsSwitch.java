/////////
// CSE 002 Craps using switch statements
// hw04
//Jorgo Damtew Tesfa, 09-10-2018
//
import java.util.Scanner;

public class CrapsSwitch{
    public static void main (String args []){
    Scanner numbScanner = new Scanner (System.in);
    
    String 
    //for the values of the two dice, the possible sums are 2,3,4,5,6,7,8,9,10,11 and 12. 
    //but the two dice could have the same value in which case they are not "easy", but "hard"
    sumTwo = "Snake Eyes",
    sumThree = "Ace Deuce", 
    sumFourDiff = "Easy Four", sumFourSame = "Hard Four",
    sumFive = "Fever Five",
    sumSixDiff = "Easy Six", sumSixSame = "Hard Six",
    sumSeven = "Seven out",
    sumEightDiff = "Easy Eight", sumEightSame = "Hard Eight",
    sumNine = "Nine",
    sumTenDiff = "Easy Ten", sumTenSame = "Hard Ten",
    sumEleven = "Yo-leven",
    sumTwelve = "Boxcars";
    
    //ask if the user wants to prvide the dice values or wants to generate random vlues
    System.out.print("If you want to roll the dice for random values, press 1.\nIf you want to pick the values of the dice, press 2.\n");
    int userWant = numbScanner.nextInt();
    
    switch (userWant) {
      case 1:
      int randDieVal1 = (int) (Math.random() * (5+1)) + 1; //randomly generating the frist die value
      int randDieVal2 = (int) (Math.random() * (5+1)) + 1; //randomly generating the second die value
      int sumRandDieVal = randDieVal1 + randDieVal2; //note that possible sums are 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 and 12
      boolean randValEquality = (randDieVal1 == randDieVal2); // testing whether the two die values are equal to each other or not
      // converting the boolean value in line 28 to integer since switch statements work with integers
      int intRandValEquality = randValEquality ? 1:0;  // 1 - true, 0 - false
        
        switch (sumRandDieVal){
          case 2: System.out.println("You got " + sumTwo + "!"); //if the the sum of the two dice values is 2
            break;
          case 3: System.out.println("You got " + sumThree + "!"); //if the sum of two dice values is 3
            break;
          case 4: 
            switch (intRandValEquality){
              case 1: System.out.println("Youy got " + sumFourSame + "!"); //if the sum of two dice values is 4 and the values are equal
                break;
              case 0: System.out.println("Youy got " + sumFourDiff + "!"); //if the sum of two dice values is 4 and the values are not equal
            }
            break;
          case 5: System.out.println("You got " + sumFive + "!"); //if the sum of two dice values is 5
            break;
          case 6:
            switch (intRandValEquality){
              case 1: System.out.println("Youy got " + sumSixSame + "!"); //if the sum of two dice values is 6 and the values are equal
                break;
              case 0: System.out.println("Youy got " + sumSixDiff + "!"); //if the sum of two dice values is 6 and the values are not equal
            }
            break;
          case 7: System.out.println("You got " + sumSeven + "!"); //if the sum of two dice values is 7
            break;
          case 8:
            switch (intRandValEquality){
              case 1: System.out.println("Youy got " + sumEightSame + "!"); //if the sum of two dice values is 8 and the values are equal
                break;
              case 0: System.out.println("Youy got " + sumEightDiff + "!"); //if the sum of two dice values is 8 and the values are not equal
            }
            break;
          case 9: System.out.println("You got " + sumNine + "!"); //if the sum of two dice values is 9
            break;
          case 10:
            switch (intRandValEquality){
              case 1: System.out.println("Youy got " + sumTenSame + "!"); //if the sum of two dice values is 10 and the values are equal
                break;
              case 0: System.out.println("Youy got " + sumTenDiff + "!"); //if the sum of two dice values is 10 and the values are not equal
            }
            break;
          case 11: System.out.println("You got " + sumEleven + "!"); //if the sum of two dice values is 11
            break;
          case 12: System.out.println("You got " + sumTwelve + "!"); //if the sum of two dice values is 12
            break;
        }
        
        break;
        
      case 2: //similar methodology as case 1, except with imput values instead of randomly generated values
        //ask the user to input the values
      System.out.print("Pick a value between 1 and 6 (inclusive) for the frist die: ");
      int inputDieVal1 = numbScanner.nextInt();
      System.out.print("Pick the value between 1 and 6 (inclusive) for the second die: ");
      int inputDieVal2 = numbScanner.nextInt();
      
      //adding up the two input values
      int sumInputDieVal = inputDieVal1 + inputDieVal2; //note that possible sums are 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 and 12
      
      boolean inputValValidity = ((inputDieVal1>=1 && inputDieVal1 <=6) && (inputDieVal2>=1 && inputDieVal2 <=6)); // checking the validity of the inputs (between 1 and 6)
      //converting the input validity boolean to intigers 1 and 0 so that we can use it in switch statement
      int intInputValValidity = inputValValidity ? 1:0; // 1 - true, 0 - false
      
      boolean inputValEquality = (inputDieVal1 == inputDieVal2); // testing whether the two die values are equal to each other or not
      // converting the boolean value in line 82 to integer since switch statements work with integers
      int intInputValEquality = inputValEquality ? 1:0;  // 1 - true, 0 - false
      
      switch (intInputValValidity){
        case 1: 
        switch (sumInputDieVal){
          case 2: System.out.println("You got " + sumTwo + "!");
            break;
          case 3: System.out.println("You got " + sumThree + "!");
            break;
          case 4: 
            switch (intInputValEquality){
              case 1: System.out.println("Youy got " + sumFourSame + "!");
                break;
              case 0: System.out.println("Youy got " + sumFourDiff + "!");
            }
            break;
          case 5: System.out.println("You got " + sumFive + "!");
            break;
          case 6:
            switch (intInputValEquality){
              case 1: System.out.println("Youy got " + sumSixSame + "!");
                break;
              case 0: System.out.println("Youy got " + sumSixDiff + "!");
            }
            break;
          case 7: System.out.println("You got " + sumSeven + "!");
            break;
          case 8:
            switch (intInputValEquality){
              case 1: System.out.println("Youy got " + sumEightSame + "!");
                break;
              case 0: System.out.println("Youy got " + sumEightDiff + "!");
            }
            break;
          case 9: System.out.println("You got " + sumNine + "!");
            break;
          case 10:
            switch (intInputValEquality){
              case 1: System.out.println("Youy got " + sumTenSame + "!");
                break;
              case 0: System.out.println("Youy got " + sumTenDiff + "!");
            }
            break;
          case 11: System.out.println("You got " + sumEleven + "!");
            break;
          case 12: System.out.println("You got " + sumTwelve + "!");
            break;
            //no defalut case, because, accordint to the boolean, both dice values are between 1 and 6, thus mun must be btween 2 to 12, inclusive
          }
          break;
        case 0: System.out.println("Invalid Entry. (I am disappointed.) Goodbye."); //if the user enters numbers other than 1,2,3,4,5 and 6
          break;
      }
        break;
        
        //if user enters a number other than 1 or 2
      default: System.out.println("Invalid Entry. Must be really hard for you to choose between 1 and 2.\nGoodbye.");
        break;
    }
  }
}