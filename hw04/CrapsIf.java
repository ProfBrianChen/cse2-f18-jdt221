/////////
// CSE 002 Craps using if statements
// hw04
//Jorgo Damtew Tesfa, 09-10-2018
//

import java.util.Scanner;

public class CrapsIf{
  public static void main (String args []){
    Scanner numbScanner = new Scanner (System.in);
    
    String
      //for the values of the two dice, the possible sums are 2,3,4,5,6,7,8,9,10,11 and 12. 
      //but the two dice could have the same value in which case they are not "easy", but "hard"
    sumTwo = "Snake Eyes",
    sumThree = "Ace Deuce", 
    sumFourDiff = "Easy Four", sumFourSame = "Hard Four",
    sumFive = "Fever Five",
    sumSixDiff = "Easy Six", sumSixSame = "Hard Six",
    sumSeven = "Seven out",
    sumEightDiff = "Easy Eight", sumEightSame = "Hard Eight",
    sumNine = "Nine",
    sumTenDiff = "Easy Ten", sumTenSame = "Hard Ten",
    sumEleven = "Yo-leven",
    sumTwelve = "Boxcars";
    
    //ask if the user wants to prvide the dice values or wants to generate random vlues
    System.out.print("If you want to roll the dice for random values, press 1.\nIf you want to pick the values of the dice, press 2.\n");
    int userWant = numbScanner.nextInt();
    
    if (userWant == 1){
      int randDieVal1 = (int) (Math.random() * (5+1)) + 1;
      int randDieVal2 = (int) (Math.random() * (5+1)) + 1;
      int sumRandDieVal = randDieVal1 + randDieVal2; //note that possible sums are 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 and 12
      if (sumRandDieVal == 2){
        System.out.println("You got " + sumTwo + "!"); //if the the sum of the two dice values is 2
      }
      else if (sumRandDieVal == 3){
        System.out.println("You got " + sumThree + "!"); //if the the sum of the two dice values is 3
      }
      else if (sumRandDieVal == 4){
        if (randDieVal1 == randDieVal2){
          System.out.println("Youy got " + sumFourSame + "!"); //if the sum of two dice values is 4 and the values are equal
        }
        else {
          System.out.println("You got " + sumFourDiff + "!"); //if the sum of two dice values is 4 and the values are not equal
        }
      }
      else if (sumRandDieVal == 5){
        System.out.println("You got " + sumFive + "!"); //if the the sum of the two dice values is 5
      }
      else if (sumRandDieVal == 6){
        if (randDieVal1 == randDieVal2){
          System.out.println("You got " + sumSixSame + "!"); //if the sum of two dice values is 6 and the values are equal
        }
        else {
          System.out.println("You got " + sumSixDiff + "!"); //if the sum of two dice values is 6 and the values are not equal
        }
      }
      else if (sumRandDieVal == 7){
        System.out.println("You got " + sumSeven + "!"); //if the the sum of the two dice values is 7
      }
      else if (sumRandDieVal == 8){
        if (randDieVal1 == randDieVal2){
          System.out.println("You got " + sumEightSame + "!"); //if the sum of two dice values is 8 and the values are equal
        }
        else {
          System.out.println("You got " + sumEightDiff + "!"); //if the sum of two dice values is 8 and the values are not equal
        }
      }
      else if (sumRandDieVal == 9){
        System.out.println("You got " + sumNine + "!"); //if the the sum of the two dice values is 8
      }
      else if (sumRandDieVal == 10){
        if (randDieVal1 == randDieVal2){
          System.out.println("You got " + sumTenSame + "!"); //if the sum of two dice values is 10 and the values are equal
        }
        else {
          System.out.println("You got " + sumTenDiff + "!"); //if the sum of two dice values is 10 and the values are not equal
        }
      }
      else if (sumRandDieVal == 11) {
        System.out.println("You got " + sumEleven + "!"); //if the the sum of the two dice values is 11
      }
      else if (sumRandDieVal == 12){
        System.out.println("You got " + sumTwelve + "!"); //if the the sum of the two dice values is 12
      }
    }
    
    else if (userWant == 2){ //similar methodology, except with imput values instead of randomly generated values
      System.out.print("Pick a value between 1 and 6 (inclusive) for the frist die: ");
      int inputDieVal1 = numbScanner.nextInt();
      System.out.print("Pick the value between 1 and 6 (inclusive) for the second die: ");
      int inputDieVal2 = numbScanner.nextInt();
      int sumInputDieVal = inputDieVal1 + inputDieVal2; //note that possible sums are 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 and 12
      
      if ((inputDieVal1>=1 && inputDieVal1 <=6) && (inputDieVal2>=1 && inputDieVal2 <=6)){ // checking the validity of the inputs (between 1 and 6)
        if (sumInputDieVal == 2){
          System.out.println("You got " + sumTwo + "!");
        }
        else if (sumInputDieVal == 3){
          System.out.println("You got " + sumThree + "!");
        }
        else if (sumInputDieVal == 4){
          if (inputDieVal1 == inputDieVal2){
            System.out.println("Youy got " + sumFourSame + "!");
          }
          else {
           System.out.println("You got " + sumFourDiff + "!");
          }
        }
        else if (sumInputDieVal == 5){
          System.out.println("You got " + sumFive + "!");
        }
       else if (sumInputDieVal == 6){
          if (inputDieVal1 == inputDieVal2){
            System.out.println("You got " + sumSixSame + "!");
         }
          else {
            System.out.println("You got " + sumSixDiff + "!");
          }
        }
       else if (sumInputDieVal == 7){
          System.out.println("You got " + sumSeven + "!");
        }
        else if (sumInputDieVal == 8){
          if (inputDieVal1 == inputDieVal2){
            System.out.println("You got " + sumEightSame + "!");
          }
          else {
            System.out.println("You got " + sumEightDiff + "!");
          }
        }
        else if (sumInputDieVal == 9){
          System.out.println("You got " + sumNine + "!");
        }
        else if (sumInputDieVal == 10){
         if (inputDieVal1 == inputDieVal2){
           System.out.println("You got " + sumTenSame + "!");
          }
          else {
            System.out.println("You got " + sumTenDiff + "!");
          }
        }
        else if (sumInputDieVal == 11) {
          System.out.println("You got " + sumEleven + "!");
        }
        else if (sumInputDieVal == 12){
          System.out.println("You got " + sumTwelve + "!");
        }
      }
    else {
      System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
    }
    }
    
    else {
      System.out.println("Invalid Entry. Must be really hard for you to choose between 1 and 2.\nGoodbye.");
    }

  }
}