//////////
/// CSE 02 Lab 02 Cyclimeter
// Jorgo Tesfa
// September 06, 2018
public class Cyclometer{
  public static void main (String [] arg){
    int secsTrip1 = 480;  // time taken for the first trip
    int secsTrip2 = 3220;  // time taken for the second trip
		int countsTrip1 = 1561;  // count of rotations for the first trip
		int countsTrip2 = 9037; // count of rotation for the second trip
    
    double wheelDiameter = 27.0,  // measure of diameter of the wheel in inches
  	PI = 3.14159, // value of Pi
  	feetPerMile = 5280,  // conversion factor between feet and mile, 5280 feet in a mile
  	inchesPerFoot = 12,   // conversion factor between inches to feet, 12 inches in a foot
  	secondsPerMinute = 60;  // conversion factor between seconds and minutes, 60 seconds in a minute
	  
    double distanceTrip1, distanceTrip2, totalDistance;  // declaring distance of trip 1, trip 2 and the total distance
    
    System.out.println("Trip 1 took " +
       	     (secsTrip1/secondsPerMinute) + " minutes and had "+ //converting secsTrip1 from seconds to minutes
       	      countsTrip1+" counts.");
	  System.out.println("Trip 2 took " + 
       	     (secsTrip2/secondsPerMinute) + " minutes and had "+ //converting secsTrip2 form seconds to minutes
       	      countsTrip2+" counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // calculating the distance of the first trip in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
    
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //calculating the distance of the second trip in miles
    
    totalDistance = distanceTrip1 + distanceTrip2; // total distance of both trips in miles
    
    //print out the values of the distances of the trips
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	  System.out.println("The total distance was " + totalDistance + " miles");

  }
}