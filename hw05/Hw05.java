/////////
// CSE 002 Hw05
// hw05
//Jorgo Damtew Tesfa, 10-08-2018
//

import java.util.Scanner;
public class Hw05 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int limit=0;//counter for thw while statement in line 29 to limit the number of hands generated
        int handVals0, handVals1, handVals2, handVals3, handVals4;
        int modHandVals0, modHandVals1, modHandVals2, modHandVals3, modHandVals4;
        int numOfPairs;
        int count4Kind = 0; //to count the number of four-of-a-kinds
        int count3Kind = 0; //to count the number of three-of-a-kinds
        int count2Pair = 0; //to count the number of 2 pairs
        int count1Pair = 0; //to count the number of 1 pair
        int numHands;
        
        System.out.print("Enter an integer for the number of hands you want to generate: ");
        while (!scan.hasNextInt()){
            System.out.println("That is not an integer.");
            System.out.print("Enter an integer for the number of hands you want to generate: ");
            String notInteger = scan.next();
        }
        numHands = scan.nextInt();

        while (limit < numHands){
            handVals0 = (int) (Math.random() * (52)) + 1;
            do{
                handVals1 = (int) (Math.random() * (52)) + 1;
            }while (handVals1==handVals0);// checks whether the number generated is equal to the ones generaed before it
            do{
                handVals2 = (int) (Math.random() * (52)) + 1;
            }while (handVals2==handVals1 || handVals2==handVals0);// checks whether the number generated is equal to the ones generaed before it
            do{
                handVals3 = (int) (Math.random() * (52)) + 1;
            }while (handVals3==handVals2 || handVals3 == handVals1 || handVals3 == handVals0);// checks whether the number generated is equal to the ones generaed before it
            do{
                handVals4 = (int) (Math.random() * (52)) + 1;
            }while (handVals4==handVals3 || handVals4==handVals2 || handVals4 == handVals1 || handVals4 == handVals0);// checks whether the number generated is equal to the ones generaed before it
            
            modHandVals0 = handVals0 % 13;
            modHandVals1 = handVals1 % 13;
            modHandVals2 = handVals2 % 13;
            modHandVals3 = handVals3 % 13;
            modHandVals4 = handVals4 % 13;
                      
            /*for (a = 0; a < 5; a++){
                for (b = a+1; b < 5; b++){
                    if (modHandVals[a] == modHandVals[b]){
                        numOfPairs++;
                    }
                }
            }*/
            numOfPairs = 0; // coutner starts dor the pairs
            if (modHandVals0 == modHandVals1){
                numOfPairs++;
            }
            if (modHandVals0 == modHandVals2){
                numOfPairs++;
            }
            if (modHandVals0 == modHandVals3){
                numOfPairs++;
            }
            if (modHandVals0 == modHandVals4){
                numOfPairs++;
            }
            if (modHandVals1 == modHandVals2){
                numOfPairs++;
            }
            if (modHandVals1 == modHandVals3){
                numOfPairs++;
            }
            if (modHandVals1 == modHandVals4){
                numOfPairs++;
            }
            if (modHandVals2 == modHandVals3){
                numOfPairs++;
            }
            if (modHandVals2 == modHandVals4){
                numOfPairs++;
            }
            if (modHandVals3 == modHandVals4){
                numOfPairs++;
            }
            /*
            Let cards be A,B,C,D,E
            if one pair, A=B
                number of pairs is 1.
            if two pairs, A=B, C=D
                number of pairs is 2.
            if three of a kind, A=B=C
                number of pairs is 3; A=B, B=C, A=C.
            if full house, A=B=C and D=E
                number of pairs is 4, A=B, B=C, A=C, D=E.
                This can be both three of a kind and two pairs.
            if four of a kind, A=B=C=D
                number of pairs is 6; A=B, A=C, A=D, B=C, B=D, C=D.
            */
            if (numOfPairs == 1){ 
                count1Pair++;
            }
            if (numOfPairs == 2 || numOfPairs ==4){
                count2Pair++;
            }
            if (numOfPairs == 3 || numOfPairs ==4){
                count3Kind++;
            }
            if (numOfPairs == 6){
                count4Kind++;
            }
            
            limit++;
        }
        
        double prob4Kind = (double)count4Kind / (double)numHands;
        double prob3Kind = (double)count3Kind / (double)numHands;
        double prob2Pair = (double)count2Pair / (double)numHands;
        double prob1Pair = (double)count1Pair / (double)numHands;
        
        System.out.println("The number of loops: " + numHands);
        System.out.printf("The probability of Four-of-a-kind: %1.3f \n", prob4Kind);
        System.out.printf("The probability of Three-of-a-kind: %1.3f \n", prob3Kind);
        System.out.printf("The probability of Two-pair: %1.3f \n", prob2Pair);
        System.out.printf("The probability of One-pair: %1.3f \n", prob1Pair);
    }  
}