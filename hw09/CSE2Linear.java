/**
 * hw 09 - CSE2 Linear and Binary searches
 * author Jorgo Damtew Tesfa jdt221
 */
import java.util.Scanner;
public class CSE2Linear {
public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int[] userInput = new int[15];
		int key;
		int iterations = 0;
		System.out.println("Enter 15 ascending ints for final grades in CSE2:"); //asking for entries
                for(int i=0;i<userInput.length;++i){
			userInput[i] = AskInput(scan,i); //using the method to ask for each entry
			if(i!=0 && userInput[i]<userInput[i-1]){
				System.out.println("Please enter a value greater than any previous value you entered!"); //instruction to make the array ascending 
				i--;
				continue;
			}
		}
		PrintArray(userInput); //priting the array
                
		System.out.print("Enter the grade to be searched: ");
		key = scan.nextInt(); //entry to be searched
		iterations = BinarySearch(userInput,key); //calling the method that searches using binary
		if(iterations != -1){ //if found, print the following
			System.out.println(key + " was found in the list with " + iterations +" iterations");
		}
		
                userInput = scramble(userInput); //scrambling the array
		System.out.println("Scrambled:");
		PrintArray(userInput); //printing the scrambled form of the array
		
                System.out.print("Enter the grade to be searched: "); 
		key = scan.nextInt(); //the grade to be searched
		iterations = LinearSearch(userInput,key); //using the linear seaching method
		if(iterations != -1){ //if found print the following 
			System.out.println(key + " was found in the list with " + iterations +" iterations");
		}
	}

	public static int AskInput(Scanner scan, int i){
		int val;
		System.out.print("Enter value number "+(i+1)+":  "); //asking for entry
		while(!scan.hasNextInt()){
			System.out.println("Invalid Entry. Enter an integer please."); //priting error
			String junk = scan.next();
			System.out.print("Enter value numebr "+(i+1)+":  "); //asking for entry
		}
		val = scan.nextInt();
		while(!(val >= 0 && val <= 100)){
			System.out.println("The entered integer is not in the specified range."); //printing error
			System.out.print("Enter value number "+(i+1)+":  "); //asking for entry
			while(!scan.hasNextInt()){
				System.out.println("Invalid Entry. Enter an integer please."); //printing error
				String junk = scan.next();
				System.out.print("Enter value number "+(i+1)+":  "); //asking for entry again
			}
			val = scan.nextInt();
		}
		return val;
	}
        
	public static void PrintArray(int[] list){
		for(int i=0; i<list.length; ++i){
			System.out.print(list[i]+" "); //pritning array vlaiues one by one
		}
		System.out.println();
	}
        
	public static int[] scramble(int[] grades){
  	int j;	
        int tempValue;
            for(int k=0;k<50;k++){ //
	  	j = (int)((Math.random()*(15))); //generating random indexes
	  	tempValue = grades[0]; //creating a temporary holder
	  	grades[0] = grades[j]; //switching values
	  	grades[j] = tempValue;
	  		
            }
  	return grades;
	}
	
	public static int BinarySearch(int[] list, int key){
		int low = 0;
		int high = list.length-1;
		int count = 0;//initializing the the number of iterations
		while(high >= low) {
			count++;//increment in the iteration
			int mid = (low + high)/2;//the middle index point
			if (key < list[mid]) {
				high = mid - 1;
			}
			else if (key == list[mid]) {
				return count;//returning the number of iterations
			}
			else {
				low = mid + 1;
			}
		}
		System.out.println(key + " was not found in the list with " + count + " iterations");//printing that the key wasn't found
		return -1;//returning -1 if key not found
	}
	
	public static int LinearSearch(int[] list, int key){
		int count = 0;//initializing the number of iterations
		for(int i=0;i<list.length;++i){
			count++;//increment in the number of iterations
			if(list[i] == key){
				return count;//returning the number of iterations in main method
			}
		}
		System.out.println(key+" was not found in the list with " + count + " iterations");//printing that the key wasn't found
		return -1;//returning -1 if the key not  found
	}
}

