/////////
// CSE 002 Pattern B
// lab06
//Jorgo Damtew Tesfa, 10-16-2018
//

import java.util.Scanner;

public class PatternB {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int pyramidLength; //variable for the length of pyramid
        int checkRange=0; //used to check the range of the input, whether it is between 1 and 10
        System.out.print("Enter an integer from 1 to 10 for length of pyramid: ");
        while (!scan.hasNextInt()){// loop to ask again if input if invalid
            System.out.println("That is not an integer.");
            System.out.print("Enter an integer from 1 to 10 for length of pyramid: ");
            String junk = scan.next(); //dump the entry to a dummy string
        }
        
        
        while (scan.hasNextInt()) {
            checkRange = scan.nextInt(); //assigning the input to checkRange to check the range
            if (checkRange<1 || checkRange>10){ //asks user again if input is not in the range of 1 to 10
            System.out.println("That is not in the range of 1 to 10.");
            System.out.print("Enter an integer from 1 to 10 for length of pyramid: ");
            }
            else if (checkRange >=1 && checkRange <=10){
                break; // if the input is between 1 and 10 it breaks out of the loop
            }
        }
        pyramidLength = checkRange; //pyramid length is assigned
        
        
        for (int numRow = pyramidLength; numRow>=1; numRow--){ //printing rows
            for (int numCol = 1; numCol<=numRow; numCol++){ //printing columns
                System.out.print(numCol);
            }
            System.out.println(); //prints new row on new line
        }
        
        
        
    }
}