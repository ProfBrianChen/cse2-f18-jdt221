/////////
// CSE 002 Arithmetic
// hw03 - Pyramid, by Jorgo Damtew Tesfa, 09-17-2018

import java.util.Scanner;
  
public class Pyramid{
  
  public static void main(String [] args){
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("The square side of the pyramid is (input length): ");
    double sidesPyramid = myScanner.nextDouble(); //entry for the length of the sides of the square at the bottom of pyramid
    
    System.out.print("The height of the pyramid is (input height): ");
    double heightPyramid = myScanner.nextDouble(); //etnry for the height of the pyramid
    
    double volumePyramid = (Math.pow(sidesPyramid,2) * heightPyramid)/3; //calculating volume of pyramid
    
    System.out.println("The volume inside the pyramid is: " + volumePyramid); 
}
} 