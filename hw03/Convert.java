/////////
// CSE 002 Arithmetic
// hw03 - Convert, by Jorgo Damtew Tesfa, 09-17-2018

import java.util.Scanner;

public class Convert{
  
  public static void main(String [] args){
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Enter the affected area in acres: ");
    double areaAffected = myScanner.nextDouble(); //entry for the number of acres of land affected by hurricane precipitation
    
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = myScanner.nextDouble(); //entry for how many inches of rain were dropped on average
    
    double sqrMilesPerAcres = 0.0015625, // how many square miles there are in an acres
    milesPerInches = 0.000015782828, // how many miles there are in a inch
    areaSqrMiles = areaAffected * sqrMilesPerAcres, // converting affected area unites from acres to square miles
    rainfallMiles = rainfall * milesPerInches, // converting rainfall from inches to miles
    cubicMiles = areaSqrMiles * rainfallMiles; // calculating the cubic miles of rainfall
    
    System.out.println(cubicMiles + " cubic miles");
  }
}