import java.util.Scanner;

public class CrapsIfPrac{
public static void main (String args []){
    Scanner numbScanner = new Scanner (System.in);
    
    //declare and assign all the possible vlaues of the game 
    String oneOne = "Sanke Eye", oneTwo = "Ace Deuce", oneThree = "Easy Four", oneFour = "Fever Five", oneFive = "Easy Six", oneSix = "Seven out",
    twoTwo = "Hard four", twoThree = "Fever five", twoFour = "Easy six", twoFive = "Seven out", twoSix = "Easy Eight",
    threeThree = "Hard six", threeFour = "Seven out", threeFive = "Easy Eight", threeSix = "Nine", 
    fourFour = "Hard Eight", fourFive = "Nine", fourSix = "Easy Ten",
    fiveFive = "Hard Ten", fiveSix = "Yo-leven", 
    sixSix = "Boxcars";
     
    //declare and assign a random number generator for the two dice
    int randRoleDie1 = (int) (Math.random() * (5+1)) + 1;
    int randRoleDie2 = (int) (Math.random() * (5+1)) + 1;
    
    //ask if the user wants to prvide the dice values or wants to generate random vlues
    System.out.print("If you want to roll the dice for random values, press 1.\nIf you want to pick the values of the dice, press 2.\n");
    int userWant = numbScanner.nextInt();
    
    //declare values for user input of the dice values
    int dieInput1, dieInput2;
    
    //if user wants to roll dice, randomly generate values and print out the conrresponsing slang terminology
    if (userWant == 1){
      //using if statements to generate the appropriate slang terminology for randomly generated values
      if (randRoleDie1 == 1 && randRoleDie2 ==1){
        System.out.println("You got " + oneOne + "!");
      }
      else if ((randRoleDie1 == 1 && randRoleDie2 ==2) || (randRoleDie1 == 2 && randRoleDie2 ==1)){
        System.out.println("You got " + oneTwo + "!");
      }
      else if ((randRoleDie1 == 1 && randRoleDie2 ==3) || (randRoleDie1 == 3 && randRoleDie2 ==1)){
        System.out.println("You got " + oneThree + "!");
      }
      else if ((randRoleDie1 == 1 && randRoleDie2 ==4) || (randRoleDie1 == 4 && randRoleDie2 ==1)){
        System.out.println("You got " + oneFour + "!");
      }
      else if ((randRoleDie1 == 1 && randRoleDie2 ==5) || (randRoleDie1 == 5 && randRoleDie2 ==1)){
        System.out.println("You got " + oneFive + "!");
      }
      else if ((randRoleDie1 == 1 && randRoleDie2 ==6) || (randRoleDie1 == 6 && randRoleDie2 ==1)){
        System.out.println("You got " + oneSix + "!");
      }
      else if (randRoleDie1 == 2 && randRoleDie2 ==2){
        System.out.println("You got " + twoTwo + "!");
      }
      else if ((randRoleDie1 == 2 && randRoleDie2 == 3) || (randRoleDie1 == 3 && randRoleDie2 == 2)){
        System.out.println("You got " + twoThree + "!");
      }
      else if ((randRoleDie1 == 2 && randRoleDie2 == 4) || (randRoleDie1 == 4 && randRoleDie2 == 2)){
        System.out.println("You got " + twoFour + "!");
      }
      else if ((randRoleDie1 == 2 && randRoleDie2 == 5) || (randRoleDie1 == 5 && randRoleDie2 == 2)){
        System.out.println("You got " + twoFive + "!");
      }
      else if ((randRoleDie1 == 2 && randRoleDie2 == 6) || (randRoleDie1 == 6 && randRoleDie2 == 2)){
        System.out.println("You got " + twoSix + "!");
      }
      else if (randRoleDie1 == 3 && randRoleDie2 == 3){
        System.out.println("You got " + threeThree + "!");
      }
      else if ((randRoleDie1 == 3 && randRoleDie2 == 4) || (randRoleDie1 == 4 && randRoleDie2 == 3)){
        System.out.println("You got " + threeFour + "!");
      }
      else if ((randRoleDie1 == 3 && randRoleDie2 == 5) || (randRoleDie1 == 5 && randRoleDie2 == 3)){
        System.out.println("You got " + threeFive + "!");
      }
      else if ((randRoleDie1 == 3 && randRoleDie2 == 6) || (randRoleDie1 == 6 && randRoleDie2 == 3)){
        System.out.println("You got " + threeSix + "!");
      }
      else if (randRoleDie1 == 4 && randRoleDie2 == 4){
        System.out.println("You got " + fourFour + "!");
      }
      else if ((randRoleDie1 == 4 && randRoleDie2 == 5) || (randRoleDie1 == 5 && randRoleDie2 == 4)){
        System.out.println("You got " + fourFive + "!");
      }
      else if ((randRoleDie1 == 4 && randRoleDie2 == 6) || (randRoleDie1 == 6 && randRoleDie2 == 4)){
        System.out.println("You got " + fourSix + "!");
      }
      else if (randRoleDie1 == 5 && randRoleDie2 == 5){
        System.out.println("You got " + fiveFive + "!");
      }
      else if ((randRoleDie1 == 5 && randRoleDie2 == 6) || (randRoleDie1 == 6 && randRoleDie2 == 5)){
        System.out.println("You got " + fiveSix + "!");
      }
      else if (randRoleDie1 == 6 && randRoleDie2 == 6){
        System.out.println("You got " + sixSix + "!");
      }
    }
    //if the user wants to input values, generate sland terminology based on the imputs
    else if (userWant == 2){
      //take the input values from the user
      System.out.print("Pick a value between 1 and 6 (inclusive) for the frist die: ");
      dieInput1 = numbScanner.nextInt();
      System.out.print("Pick the value between 1 and 6 (inclusive) for the second die: ");
      dieInput2 = numbScanner.nextInt();
      
      if (dieInput1 == 1 && dieInput2 ==1){
        System.out.println("You got " + oneOne + "!");
      }
      else if ((dieInput1 == 1 && dieInput2 ==2) || (dieInput1 == 2 && dieInput2 ==1)){
        System.out.println("You got " + oneTwo + "!");
      }
      else if ((dieInput1 == 1 && dieInput2 ==3) || (dieInput1 == 3 && dieInput2 ==1)){
        System.out.println("You got " + oneThree + "!");
      }
      else if ((dieInput1 == 1 && dieInput2 ==4) || (dieInput1 == 4 && dieInput2 ==1)){
        System.out.println("You got " + oneFour + "!");
      }
      else if ((dieInput1 == 1 && dieInput2 ==5) || (dieInput1 == 5 && dieInput2 ==1)){
        System.out.println("You got " + oneFive + "!");
      }
      else if ((dieInput1 == 1 && dieInput2 ==6) || (dieInput1 == 6 && dieInput2 ==1)){
        System.out.println("You got " + oneSix + "!");
      }
      else if (dieInput1 == 2 && dieInput2 ==2){
        System.out.println("You got " + twoTwo + "!");
      }
      else if ((dieInput1 == 2 && dieInput2 == 3) || (dieInput1 == 3 && dieInput2 == 2)){
        System.out.println("You got " + twoThree + "!");
      }
      else if ((dieInput1 == 2 && dieInput2 == 4) || (dieInput1 == 4 && dieInput2 == 2)){
        System.out.println("You got " + twoFour + "!");
      }
      else if ((dieInput1 == 2 && dieInput2 == 5) || (dieInput1 == 5 && dieInput2 == 2)){
        System.out.println("You got " + twoFive + "!");
      }
      else if ((dieInput1 == 2 && dieInput2 == 6) || (dieInput1 == 6 && dieInput2 == 2)){
        System.out.println("You got " + twoSix + "!");
      }
      else if (dieInput1 == 3 && dieInput2 == 3){
        System.out.println("You got " + threeThree + "!");
      }
      else if ((dieInput1 == 3 && dieInput2 == 4) || (dieInput1 == 4 && dieInput2 == 3)){
        System.out.println("You got " + threeFour + "!");
      }
      else if ((dieInput1 == 3 && dieInput2 == 5) || (dieInput1 == 5 && dieInput2 == 3)){
        System.out.println("You got " + threeFive + "!");
      }
      else if ((dieInput1 == 3 && dieInput2 == 6) || (dieInput1 == 6 && dieInput2 == 3)){
        System.out.println("You got " + threeSix + "!");
      }
      else if (dieInput1 == 4 && dieInput2 == 4){
        System.out.println("You got " + fourFour + "!");
      }
      else if ((dieInput1 == 4 && dieInput2 == 5) || (dieInput1 == 5 && dieInput2 == 4)){
        System.out.println("You got " + fourFive + "!");
      }
      else if ((dieInput1 == 4 && dieInput2 == 6) || (dieInput1 == 6 && dieInput2 == 4)){
        System.out.println("You got " + fourSix + "!");
      }
      else if (dieInput1 == 5 && dieInput2 == 5){
        System.out.println("You got " + fiveFive + "!");
      }
      else if ((dieInput1 == 5 && dieInput2 == 6) || (dieInput1 == 6 && dieInput2 == 5)){
        System.out.println("You got " + fiveSix + "!");
      }
      else if (dieInput1 == 6 && dieInput2 == 6){
        System.out.println("You got " + sixSix + "!");
      }
      else {
        System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
      }
    }
    //if user does not select 1 or 2, let'em know!
    else {
      System.out.println("Invalid Entry. Must be really hard for you to choose between 1 and 2.\nGoodbye.");
    }
  }
}