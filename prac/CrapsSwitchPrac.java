import java.util.Scanner;

public class CrapsSwitchPrac{
public static void main (String args []){
    Scanner numbScanner = new Scanner (System.in);
    
    //declare and assign all the possible vlaues of the game 
    String oneOne = "Sanke Eye", oneTwo = "Ace Deuce", oneThree = "Easy Four", oneFour = "Fever Five", oneFive = "Easy Six", oneSix = "Seven out",
    twoTwo = "Hard four", twoThree = "Fever five", twoFour = "Easy six", twoFive = "Seven out", twoSix = "Easy Eight",
    threeThree = "Hard six", threeFour = "Seven out", threeFive = "Easy Eight", threeSix = "Nine", 
    fourFour = "Hard Eight", fourFive = "Nine", fourSix = "Easy Ten",
    fiveFive = "Hard Ten", fiveSix = "Yo-leven", 
    sixSix = "Boxcars";
     
    //declare and assign a random number generator for the two dice
    int randRoleDie1 = (int) (Math.random() * (5+1)) + 1;
    int randRoleDie2 = (int) (Math.random() * (5+1)) + 1;
    
    //ask if the user wants to prvide the dice values or wants to generate random vlues
    System.out.print("If you want to roll the dice for random values, press 1.\nIf you want to pick the values of the dice, press 2.\n");
    int userWant = numbScanner.nextInt();
    
    //declare values for user input of the dice values
    int dieInput1, dieInput2;
    
    switch (userWant) {
      //if user presses 1, then the dice values will be randomly selected and case 1 will be executed 
      case 1:
        switch (randRoleDie1) {
          //Using switch statements for the six values of randRoleDie1 or first randomly generated die value
          case 1:
            switch (randRoleDie2) {
              //Using switch statements for the six values of randRoleDie2 or second randomly generated die value for when randRoleDie1 is 1
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneOne + "!");
                break;
              case 2: System.out.println("You got " + oneTwo + "!");
                break;
              case 3: System.out.println("You got " + oneThree + "!");
                break;
              case 4: System.out.println("You got " + oneFour + "!");
                break;
              case 5: System.out.println("You got " + oneFive + "!");
                break;
              case 6: System.out.println("You got " + oneSix + "!");
                break;
            }
            break;
          case 2:
            switch (randRoleDie2) {
              //Using switch statements for the six values of randRoleDie2 or second randomly generated die value for when randRoleDie1 is 2
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneTwo + "!");
                break;
              case 2: System.out.println("You got " + twoTwo + "!");
                break;
              case 3: System.out.println("You got " + twoThree + "!");
                break;
              case 4: System.out.println("You got " + twoFour + "!");
                break;
              case 5: System.out.println("You got " + twoFive + "!");
                break;
              case 6: System.out.println("You got " + twoSix + "!");
                break;
            }
            break;
          case 3:
            switch (randRoleDie2) {
                //Using switch statements for the six values of randRoleDie2 or second randomly generated die value for when randRoleDie1 is 3
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneThree + "!");
                break;
              case 2: System.out.println("You got " + twoThree + "!");
                break;
              case 3: System.out.println("You got " + threeThree + "!");
                break;
              case 4: System.out.println("You got " + threeFour + "!");
                break;
              case 5: System.out.println("You got " + threeFive + "!");
                break;
              case 6: System.out.println("You got " + threeSix + "!");
                break;
            }
            break;
          case 4:
            switch (randRoleDie2) {
                //Using switch statements for the six values of randRoleDie2 or second randomly generated die value for when randRoleDie1 is 4
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneFour + "!");
                break;
              case 2: System.out.println("You got " + twoFour + "!");
                break;
              case 3: System.out.println("You got " + threeFour + "!");
                break;
              case 4: System.out.println("You got " + fourFour + "!");
                break;
              case 5: System.out.println("You got " + fourFive + "!");
                break;
              case 6: System.out.println("You got " + fourSix + "!");
                break;
            }
            break;
          case 5:
            switch (randRoleDie2) {
                //Using switch statements for the six values of randRoleDie2 or second randomly generated die value for when randRoleDie1 is 5
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneFive + "!");
                break;
              case 2: System.out.println("You got " + twoFive + "!");
                break;
              case 3: System.out.println("You got " + threeFive + "!");
                break;
              case 4: System.out.println("You got " + fourFive + "!");
                break;
              case 5: System.out.println("You got " + fiveFive + "!");
                break;
              case 6: System.out.println("You got " + fiveSix + "!");
                break;
            }
            break;
          case 6:
            switch (randRoleDie2) {
                //Using switch statements for the six values of randRoleDie2 or second randomly generated die value for when randRoleDie1 is 6
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneSix + "!");
                break;
              case 2: System.out.println("You got " + twoSix + "!");
                break;
              case 3: System.out.println("You got " + threeSix + "!");
                break;
              case 4: System.out.println("You got " + fourSix + "!");
                break;
              case 5: System.out.println("You got " + fiveSix + "!");
                break;
              case 6: System.out.println("You got " + sixSix + "!");
                break;
            }
            break;
        }
        break;
      //if user presses 2, then the user has to inpur the die values and case 2 will be executed 
      case 2:
        //take the input values from the user
        System.out.print("Pick a value between 1 and 6 (inclusive) for the frist die: ");
        dieInput1 = numbScanner.nextInt();
        System.out.print("Pick the value between 1 and 6 (inclusive) for the second die: ");
        dieInput2 = numbScanner.nextInt();
        switch (dieInput1) {
            //Using switch statement for dieInput1 or the first input of the user
          case 1:
            switch (dieInput2) {
                //Using switch statement for dieInput2 or the second input of the user for when dieInput1 is 1
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneOne + "!");
                break;
              case 2: System.out.println("You got " + oneTwo + "!");
                break;
              case 3: System.out.println("You got " + oneThree + "!");
                break;
              case 4: System.out.println("You got " + oneFour + "!");
                break;
              case 5: System.out.println("You got " + oneFive + "!");
                break;
              case 6: System.out.println("You got " + oneSix + "!");
                break;
              default:
                System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
                break;
            }
            break;
          case 2:
            switch (dieInput2) {
                //Using switch statement for dieInput2 or the second input of the user for when dieInput1 is 2
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneTwo + "!");
                break;
              case 2: System.out.println("You got " + twoTwo + "!");
                break;
              case 3: System.out.println("You got " + twoThree + "!");
                break;
              case 4: System.out.println("You got " + twoFour + "!");
                break;
              case 5: System.out.println("You got " + twoFive + "!");
                break;
              case 6: System.out.println("You got " + twoSix + "!");
                break;
              default:
                System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
                break;
            }
            break;
          case 3:
            switch (dieInput2) {
                //Using switch statement for dieInput2 or the second input of the user for when dieInput1 is 3
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneThree + "!");
                break;
              case 2: System.out.println("You got " + twoThree + "!");
                break;
              case 3: System.out.println("You got " + threeThree + "!");
                break;
              case 4: System.out.println("You got " + threeFour + "!");
                break;
              case 5: System.out.println("You got " + threeFive + "!");
                break;
              case 6: System.out.println("You got " + threeSix + "!");
                break;
              default:
                System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
                break;
            }
            break;
          case 4:
            switch (dieInput2) {
                //Using switch statement for dieInput2 or the second input of the user for when dieInput1 is 4
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneFour + "!");
                break;
              case 2: System.out.println("You got " + twoFour + "!");
                break;
              case 3: System.out.println("You got " + threeFour + "!");
                break;
              case 4: System.out.println("You got " + fourFour + "!");
                break;
              case 5: System.out.println("You got " + fourFive + "!");
                break;
              case 6: System.out.println("You got " + fourSix + "!");
                break;
              default:
                System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
                break;
            }
            break;
          case 5:
            switch (dieInput2) {
                //Using switch statement for dieInput2 or the second input of the user for when dieInput1 is 5
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneFive + "!");
                break;
              case 2: System.out.println("You got " + twoFive + "!");
                break;
              case 3: System.out.println("You got " + threeFive + "!");
                break;
              case 4: System.out.println("You got " + fourFive + "!");
                break;
              case 5: System.out.println("You got " + fiveFive + "!");
                break;
              case 6: System.out.println("You got " + fiveSix + "!");
                break;
              default:
                System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
                break;
            }
            break;
          case 6:
            switch (dieInput2) {
                //Using switch statement for dieInput2 or the second input of the user for when dieInput1 is 6
                //then generating a slang terminology 
              case 1: System.out.println("You got " + oneSix + "!");
                break;
              case 2: System.out.println("You got " + twoSix + "!");
                break;
              case 3: System.out.println("You got " + threeSix + "!");
                break;
              case 4: System.out.println("You got " + fourSix + "!");
                break;
              case 5: System.out.println("You got " + fiveSix + "!");
                break;
              case 6: System.out.println("You got " + sixSix + "!");
                break;
              default:
                System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
                break;
            }
            break;
          default: //if the user enters a value outside the range of 1 to 6, this message will appear 
            System.out.println("Invalid Entry. (I'm disappointed.) Goodbye.");
            break;
        }
        break;
      default: //if user does not enter 1 or 2 to select the method of playing the game (random or picked values), the following message will appear
        System.out.println("Invalid Entry. Must be really hard for you to choose between 1 and 2.\nGoodbye.");
        break;
    }
  }
}