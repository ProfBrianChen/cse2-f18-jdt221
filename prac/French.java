//////////
/// Practice for coding and French
// Jorgo Tesfa
// September 21, 2018

import java.util.Scanner;

public class French{
  
  public static void main (String agrs []){
    //String[] verbFrench = {"avoir", "etre", "aller", "pouvoir", "faire", "partir", "vouloir", "prendre", "voir", "venir", "devoir", "manger", "savoir", "dire", "mettre", "finir", "lire", "sortir", "parler", "aimer", "boire", "recevoir", "choisir", "dormir", "connaitre", "attendre", "ecrire", "envoyer", "jouer", "falloir"};
    int n=1;
    int c=0;
    while (n <= 10) {
    int randNumb = (int) (Math.random() * (29+1)) + 1; //randomly generatring a munber from 1 to 52, inclusive
    Scanner myAnswr = new Scanner (System.in);
    
    String verbFrench = "", verbEng = "", answer; 
    
    switch (randNumb) {
      case 1: verbFrench = "avoir"; verbEng = "have";
        break;
      case 2: verbFrench = "etre"; verbEng = "be";
        break;
      case 3: verbFrench = "aller"; verbEng = "go";
        break;
      case 4: verbFrench = "faire"; verbEng = "make";
        break;
      case 5: verbFrench = "pouvoir"; verbEng = "be able to do";
        break;
      case 6: verbFrench = "partir"; verbEng = "leave";
        break;
      case 7: verbFrench = "vouloir"; verbEng = "want";
        break;
      case 8: verbFrench = "prendre"; verbEng = "take";
        break;
      case 9: verbFrench = "voir"; verbEng = "see";
        break;
      case 10: verbFrench = "venir"; verbEng = "come";
        break;
      case 11: verbFrench = "devoir"; verbEng = "have to";
        break;
      case 12: verbFrench = "manger"; verbEng = "eat";
        break;
      case 13: verbFrench = "savoir"; verbEng = "know";
        break;
      case 14: verbFrench = "dire"; verbEng = "say";
        break;
      case 15: verbFrench = "mettre"; verbEng = "put";
        break;
      case 16: verbFrench = "finir"; verbEng = "finish";
        break;
      case 17: verbFrench = "lire"; verbEng = "read";
        break;
      case 18: verbFrench = "sortir"; verbEng = "exit";
        break;
      case 19: verbFrench = "parler"; verbEng = "speak";
        break;
      case 20: verbFrench = "aimer"; verbEng = "love";
        break;
      case 21: verbFrench = "boire"; verbEng = "drink";
        break;
      case 22: verbFrench = "recevoir"; verbEng = "receive";
        break;
      case 23: verbFrench = "choisir"; verbEng = "choose";
        break;
      case 24: verbFrench = "dormir"; verbEng = "sleep";
        break;
      case 25: verbFrench = "connaitre"; verbEng = "know";
        break;
      case 26: verbFrench = "attendre"; verbEng = "wait";
        break;
      case 27: verbFrench = "ecrire"; verbEng = "write";
        break;
      case 28: verbFrench = "envoyer"; verbEng = "send";
        break;
      case 29: verbFrench = "jouer"; verbEng = "play";
        break;
      case 30: verbFrench = "falloir"; verbEng = "be necessary";
        break;
    } // for switch
    
      System.out.print(verbFrench + " means to ");
      answer = myAnswr.nextLine();
    
      if (verbEng.equals(answer)){
        System.out.println ("Correct!");
        c++;
      } // for if
      else {
        System.out.println (verbFrench + " means to " + verbEng + ".");
      } // for else
      n++;
    } // for whole loop
    System.out.println("You got " + c + " out of 10 correct.");
  }
}