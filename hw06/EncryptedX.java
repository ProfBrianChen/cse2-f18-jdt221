/*
  CSE 002 homework 06
  Encrypted X
  Jorgo Damtew Tesfa (jdt221)
  10/20/18
 */

import java.util.Scanner;
public class EncryptedX {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int sizeGrid; //variable for the size od the grid
        int checkRange=0; //used to check the range of the input, whether it is between 0 and 100
        System.out.print("Enter an integer from 0 to 100 for the size of the grid: ");
        while (!scan.hasNextInt()){// loop to ask again if input if invalid
            System.out.println("That is not an integer.");
            System.out.print("Enter an integer from 0 to 100 for the size of the grid: ");
            String junk = scan.next(); //dump the entry to a dummy string
        }
        
        while (scan.hasNextInt()) {
            checkRange = scan.nextInt(); //assigning the input to checkRange to check the range
            if (checkRange<0 || checkRange>100){ //asks user again if input is not in the range of 0 to 100
            System.out.println("That is not in the range of 0 to 100.");
            System.out.print("Enter an integer from 0 to 100 for the size of the grid: ");
            }
            else if (checkRange >=0 && checkRange <=100){
                break; // if the input is between 0 and 100 it breaks out of the loop
            }
        }
        sizeGrid = checkRange; //size of the grid is assigned
        //System.out.println(sizeGrid);
        
        for (int numRow = 0; numRow<=sizeGrid; numRow++){ //loop for the rows
            for (int numCol = 0; numCol<=sizeGrid; numCol++){ // loop for the columns 
                /* if the row number and column number are the same
                   or the row number and sizeGrid minus column number
                   are equa, print space. This creates the Encrypted X
                */
                if (numCol==numRow || (sizeGrid-numCol)==numRow){ // this if statement is to print the spaces
                  System.out.print(" ");
                }
                else {
                    System.out.print("*"); // otherwise, it prints stars to encrypt the X
                }
            }
            System.out.println(""); // to print mew line after each row
        }
    }
    
}