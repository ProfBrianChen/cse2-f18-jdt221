/*
  CSE 002 homework 07
  hw07
  Jorgo Damtew Tesfa (jdt221)
  10/30/18
 */

import java.util.Scanner;

public class hw07 {

    public static String sampleText(){
       Scanner scan = new Scanner(System.in);
       System.out.println("Enter a sample text: \n"); //prompt user to input text
       String userText = scan.nextLine();
       
       System.out.println("\nYou Entered: \n" + userText); // show user entered text
       
       return userText;
    }
    
    public static void printMenu(){ // printing the option for the menu
        System.out.println("\nMenu:\n" + 
            "c - Number of non-whitespace characters\n" +
            "w - Number of words\n" +
            "f - Find text\n" +
            "r - Replace all !'s\n" +
            "s - Shorten spaces\n" +
            "q - Quit");
        
    }
    
    public static char askUser(){ //ask user for choice from menu
        Scanner scan = new Scanner(System.in);
        char userChoice;
        do { //keep asking if entered text is not in menu
            System.out.print("\nChoose from the menu: ");
            userChoice = scan.next().charAt(0); 
        }while (userChoice != 'c'  && userChoice != 'w' && userChoice != 'f' && userChoice != 'r' && userChoice != 's' && userChoice != 'q');
        
        return userChoice;
    }
    
    public static int getNumOfNonWSCharacters(String userEntry){
        int nonWSCharacters = 0;
        
        for (int n = 0; n < userEntry.length(); ++n){
            if (!Character.isWhitespace(userEntry.charAt(n))){ //if char is not whitespace
                nonWSCharacters++; //count
            }
        }
        return nonWSCharacters;
    }
    
    public static int getNumOfWords(String userEntry){
        int numWords = 1; //start at 1 to consider the word at the end
        char space = ' ';
        userEntry = userEntry.trim();
        for (int n = 0; n < (userEntry.length()-1); ++n){
            if (userEntry.charAt(n+1) == space && !Character.isWhitespace(userEntry.charAt(n))){ 
                //-1 is added because if not, when userEntry.charAt(n+1) 
                //is written in the if statement, it will run out of characters
                numWords++; 
            }                                             
        }
        
        return numWords;
    }
    
    public static int findText(String userEntry, String desiredWord){
        int numTextFound = 0;
        
        //remove unnecessary white spaces and replace with one space
        userEntry = userEntry.replaceAll("\\s+", " "); 
        
        //count desired text
        numTextFound = userEntry.split(desiredWord).length - 1; //
        
        return numTextFound;
    }
    
    public static String replaceExclamation(String userEntry){
        //replace ! with .
        String editUserEntry = userEntry.replaceAll("!", ".");
        //return edited 
        return editUserEntry;
    }
    
    public static String shortenSpace(String userEntry){
        //replace all the whitespaces with one space
        String shortenedText = userEntry.replaceAll("\\s+", " "); 
        
        /*just in case there are some 
          spaces in front and at end.*/
        shortenedText = shortenedText.trim(); 
        
        return shortenedText;
    } 
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String userText = sampleText();
        printMenu();
        char userChoice = askUser();
        
        while (userChoice != 'q'){
            if (userChoice == 'c'){
                int nonWSCharacters = getNumOfNonWSCharacters(userText);
                System.out.print("Number of non-white space Characters: " + nonWSCharacters);
            }
            
            if (userChoice == 'w'){
                int numWords = getNumOfWords(userText);
                System.out.print("Number of words: " + numWords);
            }
            
            if (userChoice == 'f'){
                
                String desiredWord = " ";
                do {
                    System.out.print("Please enter a word or phrase to be found: ");
                    desiredWord = scan.nextLine();
                } while (desiredWord.length() <= 1);
                
                int numTextFound = findText(userText, desiredWord);
                System.out.print("\"" + desiredWord + "\" instances: " + numTextFound);
            }
            
            if (userChoice == 'r'){
                String editedText = replaceExclamation(userText);
                System.out.println("Edited text: " + editedText);
            }
            
            if (userChoice == 's'){
                String shortenedText = shortenSpace(userText);
                System.out.println("Edited text: " + shortenedText);
            }
            
        userChoice = askUser(); // ask the user again
        }
        
    }
    
}