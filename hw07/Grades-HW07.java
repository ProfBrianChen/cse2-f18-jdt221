Grade: 100
Grading Sheet for HW7

Compiles    				20 pts
Comments 				10 pts
PrintMenu Method			10 pts
Checks for Invalid Characters	10 pts
getNumOfNonWSCharacters 	10 pts
getNumOfWords			10 pts
findText				10 pts
replaceExclamation			10 pts
shortenSpace				10 pts


Take 5 points off if methods were written but did not work properly
