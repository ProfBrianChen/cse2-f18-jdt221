/////////
// CSE 002 
// lab03 - CardGenerator, by Jorgo Damtew Tesfa, 09-20-2018
//
import java.util.Random;
  
public class CardGenerator{
  
  public static void main(String [] args){
    int randCard = (int) (Math.random() * (51+1)) + 1; //randomly generatring a munber from 1 to 52, inclusive
    String cardNumb = "", cardSuit = ""; // declating the number and suit of the cards
    int randCardRemainder = randCard % 13;
    //System.out.println("Card " + randCard);

    if (randCard <=13) { //for cards beween 1 and 13
      cardSuit = "Diamonds"; //assigning the suit to this set of cards
      }
    else if (randCard >= 14 && randCard <=26) {
      cardSuit = "Clubs";
      }
    else if (randCard >= 27 && randCard <=39) {
      cardSuit = "Hearts";
      }
    else if (randCard >= 40 && randCard <=52) {
      cardSuit = "Spades";
      }
    switch (randCardRemainder) {
      case 1: cardNumb = "Ace";
        break;
      case 11: cardNumb = "Jack";
        break;
      case 12: cardNumb = "Queen";
        break;
      case 0: cardNumb = "King";
        break;
        }

    if (randCardRemainder >=2 && randCardRemainder <=10){  
      System.out.println("You picked the " + randCardRemainder + " of "+ cardSuit + ".");
    }
    else {
      System.out.println("You picked the " + cardNumb + " of "+ cardSuit + ".");
    }
  }
}
      
